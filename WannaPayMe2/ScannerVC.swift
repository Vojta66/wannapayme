//
//  ScannerVC.swift
//  WannaPayMe2
//
//  Created by INFO WEB s.r.o. on 12.02.18.
//  Copyright © 2018 INFO WEB s.r.o. All rights reserved.
//

import UIKit
import AVFoundation

var sentData = String()

class ScannerVC: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        label.text = displayData(stuff: sentData)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func returnButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func displayData(stuff : String) -> String {
        print("\nHey, I actually got something!!!\n")
        print(stuff)
        var thing = ""
        
        let array = stuff.components(separatedBy: "*")
        for component in array {
            let element = component.components(separatedBy: ":")
            let type = element[0]
            let value = element[1]
            
            switch type
            {
            case "PRE":
                if value == "xd" {break}
                thing += "Předčíslí: \(value)\n"
            case "ACC":
                thing += "Číslo účtu: \(value)\n"
            case "BANK":
                thing += "Číslo banky: \(value)\n"
            case "VS":
                if value == "xd" {break}
                thing += "Variabilní symbol: \(value)\n"
            case "KS":
                if value == "xd" {break}
                thing += "Konstantní symbol: \(value)\n"
            case "SS":
                if value == "xd" {break}
                thing += "Speciální symbol: \(value)\n"
            case "MONEY":
                thing += "Částka: \(value),- Kč\n"
            case "DATE":
                thing += "Datum splatnosti: \(value)\n"
            case "MSG":
                thing += "Zpráva pro příjemce: \(value)\n"
            default:
                thing += "I don't recognize class \"\(type)\""
            }
        }
        
        return(thing)
    }
    
    
    
}


class CameraViewContoller: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var returnBtn: UIButton!
    @IBOutlet weak var bookButton: UIButton!
    
    var video = AVCaptureVideoPreviewLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let session = AVCaptureSession()
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            session.addInput(input)
        } catch {
            print("Error.")
        }
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        video = AVCaptureVideoPreviewLayer(session: session)
        video.frame = view.bounds
        view.layer.addSublayer(video)
        
        
        session.startRunning()
        self.view.bringSubview(toFront: returnBtn)
        // Moved to viewDidAppear
        //if sentData != "" {
        //    self.view.bringSubview(toFront: bookButton)
        //}
        
        
    }
    
    
    
    // Triggered when QR code is detected (when we get an output)
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects != nil && metadataObjects.count != 0
        {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
            {
                let thing = object.stringValue
                print("\n\(thing!)\n")
                
                let msg = decode(qrString: thing!)
                
                let alert = UIAlertController(title: "QR Code", message: "Do you want to use code with message \"\(msg)\"?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Throw away", style: UIAlertActionStyle.default, handler: nil))
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (nil) in
                    print("Accepted.")
                    sentData = thing!
                    print(sentData)
                    self.performSegue(withIdentifier: "segue", sender: self)
                }))
                present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var sumVC = segue.destination as! ScannerVC
        sumVC.displayData(stuff: sentData)
    }
    
    func decode(qrString: String) -> String{
        
        // DECODING
        
        let arr = qrString.components(separatedBy: "*")
        for item in arr
        {
            let thing = item.components(separatedBy: ":")
            if thing.count > 1
            {
                let type = thing[0]
                let value = thing[1]
                switch type
                {
                case "MSG":
                    print("Message: \(value)")
                    return value
                default:
                    print("Skipped class \"\(type)\"")
                }
            }
        }
        return "rip"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if sentData != "" {
            self.view.bringSubview(toFront: bookButton)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func returnButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
