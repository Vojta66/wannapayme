//
//  GeneratorVC.swift
//  WannaPayMe2
//
//  Created by INFO WEB s.r.o. on 12.02.18.
//  Copyright © 2018 INFO WEB s.r.o. All rights reserved.
//

import UIKit
import Foundation

var globalQrThing = ""

class GeneratorVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var preNrTxt: UITextField!
    @IBOutlet weak var accNrTxt: UITextField!
    @IBOutlet weak var bankNrTxt: UITextField!
    @IBOutlet weak var amountTxt: UITextField!
    @IBOutlet weak var vsTxt: UITextField!
    @IBOutlet weak var ssTxt: UITextField!
    @IBOutlet weak var ksTxt: UITextField!
    
    @IBOutlet weak var dayTxt: UITextField!
    @IBOutlet weak var monthTxt: UITextField!
    @IBOutlet weak var yearTxt: UITextField!
    
    @IBOutlet weak var messageTxt: UITextField!
    
    @IBAction func clearButton(_ sender: Any) {
        preNrTxt.text = ""
        accNrTxt.text = ""
        bankNrTxt.text = ""
        amountTxt.text = ""
        vsTxt.text = ""
        ssTxt.text = ""
        ksTxt.text = ""
        dayTxt.text = ""
        monthTxt.text = ""
        yearTxt.text = ""
        messageTxt.text = ""
    }
    
    @IBAction func generateButton(_ sender: Any) {
        globalQrThing = ""
        
        if ((accNrTxt.text?.characters.count)! < 9 ||  (accNrTxt.text?.characters.count)! > 10 || bankNrTxt.text?.characters.count != 4 || dayTxt.text! == "" || monthTxt.text! == "" || yearTxt.text?.characters.count != 4) {
            return
        }
        
       
        let preNumber = preNrTxt.text!
        if preNumber != "" {
            globalQrThing += ("PRE:" + preNumber)
        }
        var accNumber = accNrTxt.text!
        if accNumber.characters.count == 9 {
            accNumber = "0" + accNumber
        }
        globalQrThing += ("*ACC:" + accNumber)
        let bankNumber = bankNrTxt.text!
            globalQrThing += ("*BANK:" + bankNumber)
        var money = amountTxt.text!
        if money == "" {
            money = "0"
        }
        globalQrThing += ("*MONEY:" + money)
        var vs = vsTxt.text!
        if vs == "" {
            vs = "xd"
        }
        globalQrThing += ("*VS:" + vs)
        var ss = ssTxt.text!
        if ss == "" {
            ss = "xd"
        }
        globalQrThing += ("*SS:" + ss)

        var ks = ksTxt.text!
        if ks == "" {
            ks = "xd"
        }
        globalQrThing += ("*KS:" + ks)
        
        var day = dayTxt.text!
        var month = monthTxt.text!
        let year = yearTxt.text!
        globalQrThing += "*DATE:\(day).\(month).\(year)"
        var message = messageTxt.text!
        if message == "" {
            message = "xd"
        }
        globalQrThing += ("*MSG:" + message)
        
        
        // Presenting another ViewController programmatically
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "qrStoryboardID") as! QRViewController
        present(vc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("viewDidLoad")
        
        let datum = Date()
        let kalendar = Calendar.current
        
        dayTxt.text = String(kalendar.component(.day, from: datum))
        monthTxt.text = String(kalendar.component(.month, from: datum))
        yearTxt.text = String(kalendar.component(.year, from: datum))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Clearing text field when editing began
    @IBAction func clearDayTextField(_ sender: Any) {
    dayTxt.text = ""}
    
    @IBAction func clearMonthTextField(_ sender: Any) {
    monthTxt.text = ""}
    
    @IBAction func clearYearTextField(_ sender: Any) {
        yearTxt.text = ""
    }
    
    // Hiding keyboard 
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        messageTxt.resignFirstResponder()
        return(true)
    }
    
    @IBAction func returnButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   

}

// *** ANOTHER VIEW CONTROLLER ***

class QRViewController: UIViewController {
    
    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var sizeSlider: UISlider!
    
    var qrCodeImage : CIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CreateQRCode(globalQrThing)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func CreateQRCode(_ qr: String) {
        print(qr)
        let data = qr.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(data, forKey: "inputMessage")
        filter?.setValue("Q", forKey: "inputCorrectionLevel")
        
        qrCodeImage = filter?.outputImage
        
        // imgQRCode.image = UIImage(ciImage: (filter?.outputImage)!) (blurry)
        displayQRCodeImage()
    }
    
    func displayQRCodeImage() {
        let scaleX = imgQRCode.frame.size.width / qrCodeImage.extent.size.width
        let scaleY = imgQRCode.frame.size.width / qrCodeImage.extent.size.width
        
        let transformedImage = qrCodeImage.transformed(by: CGAffineTransform.init(scaleX: scaleX, y: scaleY))
        
        imgQRCode.image = UIImage(ciImage: transformedImage)
    }
    @IBAction func changeSizeSliderAction(_ sender: Any) {
        imgQRCode.transform = CGAffineTransform(scaleX: CGFloat(sizeSlider.value), y: CGFloat(sizeSlider.value))
    }
    
    @IBAction func returnButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
